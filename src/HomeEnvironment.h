#ifndef HomeEnvironment_H
#define HomeEnvironment_H

#include <Arduino.h>

#if defined(ESP8266) || defined(ESP8285)
#include <ESP8266WiFi.h>
#else
#include <WiFi.h>
#endif

#define MQTT_KEEPALIVE 60      // 60 seconds
#define MQTT_SOCKET_TIMEOUT 60 // 60 seconds

#include <ArduinoJson.h>
#include <WiFiClientSecure.h>
#include "PubSubClient.h"
#include <NTPClient.h>
#include <WiFiUdp.h>

#if defined(ESP8266) || defined(ESP32)
#include <functional>
#define CONNECTED_CALLBACK_SIGNATURE std::function<void(HomeEnvironment *)> connection_callback
#else
#define CONNECTED_CALLBACK_SIGNATURE void (*connection_callback)()
#endif

#define CONFIG_FILE "/configuration.json"

#define DEFAULT_TIMESERVER "europe.pool.ntp.org"
#define DEFAULT_TIMEZONE "CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00"
#define DEFAULT_TIME_OFFSET (3 * 3600)

class HomeEnvironment
{
private:
   CONNECTED_CALLBACK_SIGNATURE;
   MQTT_CALLBACK_SIGNATURE;

   WiFiUDP ntpUDP;
   NTPClient *timeClient = nullptr;

   WiFiClientSecure secureClient;
   PubSubClient *client = nullptr;

   boolean setClock();
   boolean clock_set = false;

   bool fadeOn = false;
   bool fadeOff = true;

   long fadeMillis = 0;

   int fade = 1024;
   int avg_time_ms;

#if defined(ESP8266) || defined(ESP8285)
   BearSSL::X509List *x509CaCert, *x509ClientCert;
   BearSSL::PrivateKey *PrivateClientKey;
#endif

   char *caBuffer, *certBuffer, *privateKeyBuffer;

   long lastActivity = 0;
   long reconnectTimeout = 120000; // 2 minues in millis

   long pingInterval = 0;
   long connectionRetryInterval = 0;
   long statisticsInterval = 0;
   int pingFailureInterval = 0;

   bool lastLoop = false;

   boolean connectToWifi();
   boolean connectToMqtt();

   boolean deepSleepEnabled = false;
   boolean debugOutput = false;

public:
   HomeEnvironment();
   HomeEnvironment(const char *ssid, const char *pass, const char *hostname, const char *ota_password);
   HomeEnvironment(const char *ssid, const char *pass, const char *host, uint16_t port, const char *client_id, const char *ota_password);

   boolean initialize();
   boolean initialize(const char *available_topic, const char *debug_topic);
   void begin();
   void loop();
   void setReconnectTimeout(long timeout);
   void setHeartbeat(bool heartbeat);   
   void freezeWithOTA(char *reason);
   void setOnConnectionCallback(CONNECTED_CALLBACK_SIGNATURE);
   void setLedPin(uint8_t gpio);
   void setDebug(bool debug);
   
   bool otaConfigCallback(byte *load, unsigned int length);
   
#if defined(ESP8266) || defined(ESP8285)
   const void onWiFiEvent(WiFiEvent_t event);
#elif defined(ESP32)
   const void onWiFiEvent(WiFiEvent_t event, WiFiEventInfo_t info);
#endif
   String getClientId();
   String getOtaConfigTopic();

   boolean setBufferSize(uint16_t size);

   boolean connected();

   bool setCallback(MQTT_CALLBACK_SIGNATURE);

   boolean publish(const char *topic, const char *payload);
   boolean publish(const char *topic, const char *payload, boolean retained);
   boolean publish(const char *topic, const uint8_t *payload, unsigned int plength);
   boolean publish(const char *topic, const uint8_t *payload, unsigned int plength, boolean retained);
   boolean publish_P(const char *topic, const char *payload, boolean retained);
   boolean publish_P(const char *topic, const uint8_t *payload, unsigned int plength, boolean retained);
   // Start to publish a message.
   // This API:
   //   beginPublish(...)
   //   one or more calls to write(...)
   //   endPublish()
   // Allows for arbitrarily large payloads to be sent without them having to be copied into
   // a new buffer and held in memory at one time
   // Returns 1 if the message was started successfully, 0 if there was an error
   boolean beginPublish(const char *topic, unsigned int plength, boolean retained);
   // Finish off this publish message (started with beginPublish)
   // Returns 1 if the packet was sent successfully, 0 if there was an error
   int endPublish();
   // Write a single byte of payload (only to be used with beginPublish/endPublish)
   virtual size_t write(uint8_t);
   // Write size bytes from buffer into the payload (only to be used with beginPublish/endPublish)
   // Returns the number of bytes written
   virtual size_t write(const uint8_t *buffer, size_t size);
   boolean subscribe(const char *topic);
   boolean subscribe(const char *topic, uint8_t qos);
   boolean unsubscribe(const char *topic);

private:
   void printStatistics(); // Prints the internal statistics
   
   bool getConfig(DynamicJsonDocument &root);
   bool updateConfig(char* buffer, size_t length, bool dryRun = true);
   bool updateConfig(JsonObject &root, bool dryRun = false);
   bool writeConfig(DynamicJsonDocument &root);   
};

#endif
