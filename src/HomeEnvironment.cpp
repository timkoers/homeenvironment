#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

#if defined(ESP8266) || defined(ESP8285)
#warning("ESP8266 Detected")
#include "logs.h"

// Define the log macro's here
#define ESP_LOGE(TAG, ...) logerror(__VA_ARGS__)
#define ESP_LOGW(TAG, ...) logwarn(__VA_ARGS__)
#define ESP_LOGI(TAG, ...) loginfo(__VA_ARGS__)
#define ESP_LOGD(TAG, ...) logdebug(__VA_ARGS__)
#define ESP_LOGV(TAG, ...) logtrace(__VA_ARGS__)

#endif

#ifdef ESP32
#warning("ESP32 Detected")
#include "esp_log.h"
#endif

#define LOG_TAG "HomeEnvironment"

#include "HomeEnvironment.h"
#include "Arduino.h"

#if defined(ESP8266) || defined(ESP8285)
#define WDT_TIMEOUT 5000 // seconds
#include <ESP8266Ping.h>
#endif

#ifdef ESP32
#define FEED_WDT() ESP_ERROR_CHECK(esp_task_wdt_reset())
#define WDT_TIMEOUT 5 // seconds
#include <ESP32Ping.h>
#include <esp_task_wdt.h>
#endif

#if defined(ESP8266) || defined(ESP8285)
#include <ESP8266WiFi.h>
#define FEED_WDT()     \
	{                  \
		ESP.wdtFeed(); \
		yield();       \
	}
#endif
#ifdef ESP32
#include <WiFi.h>
#endif

#include <ArduinoJson.h>
#include <StreamUtils.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <time.h>

#ifndef DISABLE_OTA
#include <ArduinoOTA.h>
#else
#warning("Disabling OTA!")
#endif

#if defined(ESP8266) || defined(ESP8285)
#include "LittleFS.h" // LittleFS is declared
#define FSPROVIDER LittleFS
#elif defined(ESP32)
#include "SPIFFS.h"
#define FSPROVIDER SPIFFS
#define delay(A) vTaskDelay(pdMS_TO_TICKS(A)) // Change the delay function to the task delay
#endif

#define FADE_DELAY 1
#define PING_INTERVAL_TIMING 10000 // 10 seconds
#define STATISTICS_INTERVAL 2000   // 200 seconds
#define FAILED_RETRY_INTERVAL 5000 // 5 seconds
#define CLOCK_ACQ_FAIL_COUNT 5	   // 5 times

#if defined(ESP8266) || defined(ESP8285)
void littleFsListDir(const char *dirname)
{
	ESP_LOGV(LOG_TAG, "Listing directory: %s", dirname);

	Dir root = FSPROVIDER.openDir(dirname);

	while (root.next())
	{
		FEED_WDT();

		File file = root.openFile("r");
		ESP_LOGV(LOG_TAG, "FILE: %s", root.fileName().c_str());
		ESP_LOGV(LOG_TAG, "  SIZE: %d", file.size());
		time_t cr = file.getCreationTime();
		time_t lw = file.getLastWrite();
		file.close();
		struct tm *tmstruct = localtime(&cr);
		ESP_LOGV(LOG_TAG, "    CREATION: %d-%02d-%02d %02d:%02d:%02d", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
		tmstruct = localtime(&lw);
		ESP_LOGV(LOG_TAG, "  LAST WRITE: %d-%02d-%02d %02d:%02d:%02d", (tmstruct->tm_year) + 1900, (tmstruct->tm_mon) + 1, tmstruct->tm_mday, tmstruct->tm_hour, tmstruct->tm_min, tmstruct->tm_sec);
	}
}
#endif

namespace Config
{
	namespace Placeholders
	{
		const String kClientId = "{client_id}";
		const String kOtaPassword = "{ota_password}";
	}

	namespace Members
	{
		const char *kClientId = "client_id";
		const char *kMqttHostname = "mqtt_hostname";
		const char *kMqttPort = "mqtt_port";
		const char *kOtaPassword = "ota_password";
		const char *kOtaPort = "ota_port";
		const char *kWiFiSsid = "wifi_ssid";
		const char *kWiFiPassword = "wifi_password";
		const char *kTimeZone = "time_zone";
		const char *kTimeServer = "time_server";
		const char *kTimeGmtOffset = "time_gmt_offset";
		const char *kAvailableTopic = "available_topic";
		const char *kDebugTopic = "debug_topic";
		const char *kHeartbeat = "heartbeat";
		const char *kHeartbeatLed = "heartbeat_led";
	} // namespace Members

	String client_id;
	String available_topic;
	String state_topic;
	String debug_topic;
	String ssid;
	String pass;
	String host = "";
	String ota_password;
	String time_zone = DEFAULT_TIMEZONE;
	String time_server = DEFAULT_TIMESERVER;

	String otaConfigTopic = "{client_id}/OTA/{ota_password}";
	String otaConfigState = "{client_id}/OTA/State";

	long time_gmt_offset = DEFAULT_TIME_OFFSET;

	uint16_t port;
	uint16_t ota_port;

#ifndef LED_BUILTIN
#define LED_BUILTIN 255
#endif // LED_BUILTIN

	uint8_t led = LED_BUILTIN;
	bool heartBeat = true;

} // namespace Config

HomeEnvironment::HomeEnvironment()
{
	initialize();
}

HomeEnvironment::HomeEnvironment(const char *&ssid, const char *&pass, const char *hostname, const char *ota_password) : HomeEnvironment(ssid, pass, "", 0, hostname, ota_password)
{
}

HomeEnvironment::HomeEnvironment(const char *&ssid, const char *&pass, const char *host, uint16_t port, const char *client_id, const char *ota_password)
{
	Config::client_id = (char *)client_id;
	Config::ssid = (char *)ssid;
	Config::pass = (char *)pass;
	Config::host = (char *)host;
	Config::port = port;
	Config::ota_password = (char *)ota_password;
}

// Deprecated
boolean HomeEnvironment::initialize(const char *available_topic, const char *debug_topic)
{
	// Backwards compatibility
	if (client != nullptr)
		return true;

		// Check if the config.json exists and write the topics to the filesystem.

		// Open the FileSystem and load the config file
#if defined(ESP8266) || defined(ESP8285)

	log_setserial(&Serial);

	ESP_LOGV(LOG_TAG, "[%d]: Opening ESP8266 SPIFFS", millis());
	if (!FSPROVIDER.begin())
	{
#elif defined(ESP32)
	ESP_LOGD(LOG_TAG, "[%d]: Opening ESP32 FS", millis());
	if (!SPIFFS.begin(true))
	{
#endif //ESP32
		ESP_LOGE(LOG_TAG, "[%d]: Spiffs could not be opened", millis());
	}
	else
	{
		littleFsListDir("/");

		// Load the Json file here
		DynamicJsonDocument root(4096);
		getConfig(root);

		// Check if the rest is already set, then set it
		if (strlen(available_topic) > 0 && !root.containsKey(Config::Members::kAvailableTopic))
			root[Config::Members::kAvailableTopic] = String(available_topic);

		if (strlen(debug_topic) > 0 && !root.containsKey(Config::Members::kDebugTopic))
			root[Config::Members::kDebugTopic] = String(debug_topic);

		if (!Config::client_id.isEmpty() && !root.containsKey(Config::Members::kClientId))
			root[Config::Members::kClientId] = Config::client_id;

		if (!Config::ssid.isEmpty() && !root.containsKey(Config::Members::kWiFiSsid))
			root[Config::Members::kWiFiSsid] = Config::ssid;

		if (!Config::pass.isEmpty() && !root.containsKey(Config::Members::kWiFiPassword))
			root[Config::Members::kWiFiPassword] = Config::pass;

		if (!Config::host.isEmpty() && !root.containsKey(Config::Members::kMqttHostname))
			root[Config::Members::kMqttHostname] = Config::host;

		if (Config::port != 0 && !root.containsKey(Config::Members::kMqttPort))
			root[Config::Members::kMqttPort] = Config::port;

		if (!Config::time_server.isEmpty() && !root.containsKey(Config::Members::kTimeServer))
			root[Config::Members::kTimeServer] = Config::time_server;

		if (!Config::time_zone.isEmpty() && !root.containsKey(Config::Members::kTimeZone))
			root[Config::Members::kTimeZone] = Config::time_zone;

		if (Config::time_gmt_offset != 0 && !root.containsKey(Config::Members::kTimeGmtOffset))
			root[Config::Members::kTimeGmtOffset] = Config::time_gmt_offset;

		ESP_LOGI("HomeEnvironment", "Writing config");

		writeConfig(root);
	}

	return initialize();
}

boolean HomeEnvironment::initialize()
{
	bool ok = false;

#if defined(ESP8266) || defined(ESP8285)
	ESP.wdtEnable(WDT_TIMEOUT);
	ESP.wdtFeed();
	yield();

	log_setserial(&Serial);

#elif defined(ESP32)
	esp_task_wdt_init(WDT_TIMEOUT, true); //enable panic so ESP32 restarts
	esp_task_wdt_add(NULL);				  //add current thread to WDT watch
	esp_task_wdt_reset();
	// ESP32
#endif

	// Open the FileSystem and load the config file
#if defined(ESP8266) || defined(ESP8285)
	ESP_LOGV(LOG_TAG, "[%d]: Opening ESP8266 SPIFFS", millis());
	if (!FSPROVIDER.begin())
	{
#elif defined(ESP32)
	ESP_LOGD(LOG_TAG, "[%d]: Opening ESP32 FS", millis());
	if (!SPIFFS.begin(true))
	{
#endif //ESP32
		ESP_LOGE(LOG_TAG, "[%d]: Spiffs could not be opened", millis());
	}
	else
	{		
		// Load the Json file here
		DynamicJsonDocument root(4096);

		if (getConfig(root))
		{
			// Read everything into the class variables
			Config::client_id = root.containsKey(Config::Members::kClientId) ? root[Config::Members::kClientId].as<String>() : "";
			Config::host = root.containsKey(Config::Members::kMqttHostname) ? root[Config::Members::kMqttHostname].as<String>() : "";
			Config::port = root.containsKey(Config::Members::kMqttPort) ? root[Config::Members::kMqttPort].as<uint16_t>() : 0;
			Config::ota_password = root.containsKey(Config::Members::kOtaPassword) ? root[Config::Members::kOtaPassword].as<String>() : "";
			Config::ota_port = root.containsKey(Config::Members::kOtaPort) ? root[Config::Members::kOtaPort].as<uint16_t>() : 0;
			Config::ssid = root.containsKey(Config::Members::kWiFiSsid) ? root[Config::Members::kWiFiSsid].as<String>() : "";
			Config::pass = root.containsKey(Config::Members::kWiFiPassword) ? root[Config::Members::kWiFiPassword].as<String>() : "";
			Config::time_server = root.containsKey(Config::Members::kTimeServer) ? root[Config::Members::kTimeServer].as<String>() : Config::time_server;
			Config::time_zone = root.containsKey(Config::Members::kTimeZone) ? root[Config::Members::kTimeZone].as<String>() : Config::time_zone;
			Config::time_gmt_offset = root.containsKey(Config::Members::kTimeGmtOffset) ? root[Config::Members::kTimeGmtOffset].as<long>() : 0;
			Config::available_topic = root.containsKey(Config::Members::kAvailableTopic) ? root[Config::Members::kAvailableTopic].as<String>() : "";
			Config::heartBeat = root.containsKey(Config::Members::kHeartbeat) ? root[Config::Members::kHeartbeat].as<bool>() : false;
			Config::led = root.containsKey(Config::Members::kHeartbeatLed) ? root[Config::Members::kHeartbeatLed].as<uint8_t>() : 0;

			// Replace the placeholders
			Config::available_topic.replace(Config::Placeholders::kClientId, Config::client_id);
			Config::debug_topic.replace(Config::Placeholders::kClientId, Config::client_id);

			Config::otaConfigTopic.replace(Config::Placeholders::kClientId, Config::client_id);
			Config::otaConfigTopic.replace(Config::Placeholders::kOtaPassword, Config::ota_password);

			Config::otaConfigState.replace(Config::Placeholders::kClientId, Config::client_id);
		}

		ESP_LOGI(LOG_TAG, "[%d]: HomeEnvironment initialized with %s, %s, %s:%02u", millis(),
				 Config::client_id.c_str(), Config::ssid.c_str(), Config::host.c_str(), Config::port);

		if (!Config::host.isEmpty())
		{
			const int buffer_size = 64;

			char *crt_filename = (char *)malloc(buffer_size);
			memset(crt_filename, 0, buffer_size);

			sprintf(crt_filename, "/%s.crt", Config::client_id.c_str());

			char *pk_filename = (char *)malloc(buffer_size);
			memset(pk_filename, 0, buffer_size);

			sprintf(pk_filename, "/%s.key", Config::client_id.c_str());

			File ca = FSPROVIDER.open("/ca.crt", "r");

			if (!ca)
			{
				ESP_LOGE(LOG_TAG, "[%d]: ca.crt could not be opened");
#if defined(ESP8266) || defined(ESP8285)
				littleFsListDir("/");
#endif
				freezeWithOTA("ca.crt could not be openend");
			}

			File cert = FSPROVIDER.open(crt_filename, "r");
			if (!cert)
			{
				ESP_LOGE(LOG_TAG, "[%d]: %s could not be opened", millis(), crt_filename);
				freezeWithOTA("Cert could not be opened\n");
			}

			File key = FSPROVIDER.open(pk_filename, "r");
			if (!key)
			{
				ESP_LOGE(LOG_TAG, "[%d]: %s could not be opened", millis(), pk_filename);
				freezeWithOTA("Key could not be opened\n");
			}

			FEED_WDT();

			caBuffer = (char *)malloc(ca.size() + 1);
			int read = 0;

			if ((read = ca.readBytes(caBuffer, ca.size())) != ca.size())
			{
				freezeWithOTA("CA read failed\n");
				ESP_LOGE(LOG_TAG, "CA Read %d bytes, expected %d bytes", read, ca.size());
			}

			caBuffer[ca.size()] = '\0';

			certBuffer = (char *)malloc(cert.size() + 1);

			if ((read = cert.readBytes(certBuffer, cert.size())) != cert.size())
			{
				ESP_LOGE(LOG_TAG, "cert Read %d bytes, expected %d bytes", read, cert.size());
				freezeWithOTA("Cert read failed\n");
			}

			certBuffer[cert.size()] = '\0';

			privateKeyBuffer = (char *)malloc(key.size() + 1);

			if ((read = key.readBytes(privateKeyBuffer, key.size())) != key.size())
			{
				ESP_LOGE(LOG_TAG, "key Read %d bytes, expected %d bytes", read, key.size());
				freezeWithOTA("key read failed\n");
			}

			privateKeyBuffer[key.size()] = '\0';

			FEED_WDT();

#if defined(ESP8266) || defined(ESP8285)
			ESP_LOGD(LOG_TAG, "[%d]: Setting BearSSL certificate things", millis());

			x509CaCert = new BearSSL::X509List(caBuffer);
			x509ClientCert = new BearSSL::X509List(certBuffer);
			PrivateClientKey = new BearSSL::PrivateKey(privateKeyBuffer);

			secureClient.setTrustAnchors(x509CaCert);
			secureClient.setClientRSACert(x509ClientCert, PrivateClientKey);
#elif defined(ESP32)
			ESP_LOGD(LOG_TAG, "[%d] Setting ESP32 certificate things", millis());

			secureClient.setCACert(caBuffer);
			secureClient.setCertificate(certBuffer);
			secureClient.setPrivateKey(privateKeyBuffer);
#endif // ESP32

			// Delete the old one
			if (client != nullptr)
				delete (client);

			client = new PubSubClient(Config::host.c_str(), Config::port, secureClient);

			ESP_LOGD(LOG_TAG, "[%d]: Free heap memory, before freeing buffers: %d", millis(), ESP.getFreeHeap());

			// Delete all the buffers, except the caBuffer, which is used and can't be deleted
			delete[] crt_filename;
			delete[] pk_filename;
		}

		ESP_LOGD(LOG_TAG, "[%d]: Free heap memory: %d", millis(), ESP.getFreeHeap());

		// In each loop, make sure there is an Internet connection.
		if ((ok = WiFi.status() == WL_CONNECTED), !ok)
		{
			ok = connectToWifi();
		}

		if (ok && !clock_set)
		{
			ESP_LOGV(LOG_TAG, "[%d]: Clock not set, setting", millis());
			ok = clock_set = setClock();
		}
#ifndef DISABLE_OTA
		String otaHostname = String(Config::client_id);
		otaHostname.replace("-", "");
		otaHostname.replace("_", "");
		otaHostname.replace(" ", "");

		ArduinoOTA.setHostname(otaHostname.c_str());
		ArduinoOTA.setPort(Config::ota_port);
		//#if defined(ESP8266) || defined(ESP8285)
		//	ArduinoOTA.setPort(8266);
		//#endif // defined(ESP8266) || defined(ESP8285)
		//#ifdef ESP32
		//	ArduinoOTA.setPort(32);
		//#endif //ESP32

		ArduinoOTA.setPassword(Config::ota_password.c_str());

		ArduinoOTA.onStart([]() {
			noInterrupts();
			String type;
			if (ArduinoOTA.getCommand() == U_FLASH)
			{
				type = "sketch";
			}
			else
			{ // U_SPIFFS
				type = "filesystem";
				FSPROVIDER.end();
			}
		});

		ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
			// TODO Implement a user function callback
			//digitalWrite(led, !digitalRead(led));
		});

		ArduinoOTA.begin();
#endif

		// Subscribe to the config update topic here
		subscribe(Config::otaConfigTopic.c_str());

		ESP_LOGV(LOG_TAG, "[%d]: Free heap memory: %d", millis(), ESP.getFreeHeap());
		return ok;
	}
	return false;
}

boolean HomeEnvironment::connected()
{
	if (client == nullptr)
		return false;
	return client->connected();
}

void HomeEnvironment::setLedPin(uint8_t gpio)
{
	Config::led = gpio;
}

void HomeEnvironment::setOnConnectionCallback(CONNECTED_CALLBACK_SIGNATURE)
{
	this->connection_callback = connection_callback;
}

void HomeEnvironment::setReconnectTimeout(long timeout)
{
	this->reconnectTimeout = timeout;
}

void HomeEnvironment::setDebug(bool debug)
{
	this->debugOutput = debug;
}

void HomeEnvironment::setHeartbeat(bool heartbeat)
{
	Config::heartBeat = heartbeat;
	digitalWrite(Config::led, LOW);
}

boolean HomeEnvironment::setBufferSize(uint16_t size)
{
	if (client == nullptr)
		return false;

	return client->setBufferSize(size);
}

#if defined(ESP8266) || defined(ESP8285)
#define STA_GOT_IP WIFI_EVENT_STAMODE_GOT_IP
#define AUTOMODE_CHANGE WIFI_EVENT_MODE_CHANGE
#define STA_DISCONNECTED WIFI_EVENT_STAMODE_DISCONNECTED
#define LOST_IP WIFI_EVENT_STAMODE_DHCP_TIMEOUT
#elif defined(ESP32)
#define STA_GOT_IP SYSTEM_EVENT_STA_GOT_IP
#define AUTOMODE_CHANGE SYSTEM_EVENT_STA_AUTHMODE_CHANGE
#define STA_DISCONNECTED SYSTEM_EVENT_STA_DISCONNECTED
#define LOST_IP SYSTEM_EVENT_STA_LOST_IP
#endif

#if defined(ESP8266) || defined(ESP8285)
const void HomeEnvironment::onWiFiEvent(WiFiEvent_t event)
#elif defined(ESP32)
const void HomeEnvironment::onWiFiEvent(WiFiEvent_t event, WiFiEventInfo_t info)
#endif
{
	switch (event)
	{
	case STA_GOT_IP:
	{
		connectToMqtt();
		break;
	}

	case AUTOMODE_CHANGE:
	{
		// Fall through
	}

	case STA_DISCONNECTED:
	{
		// Fall through
	}

	case LOST_IP:
	{
		connectToWifi();
		break;
	}
	}
}

void HomeEnvironment::printStatistics()
{
	time_t now = time(nullptr);
	struct tm timeinfo;
	gmtime_r(&now, &timeinfo);

	ESP_LOGI(LOG_TAG, "[%d]: Pinged %s 2 times, %d ms, pinging failed %d times ", millis(), Config::host.c_str(), avg_time_ms, pingFailureInterval);
	if (!Config::host.isEmpty())
		ESP_LOGI(LOG_TAG, "[%d]: MQTT Client state %d", millis(), client->state());
	ESP_LOGI(LOG_TAG, "[%d]: Free heap memory: %d", millis(), ESP.getFreeHeap());
	ESP_LOGI(LOG_TAG, "[%d]: Current time (UTC) %s", millis(), asctime(&timeinfo)); // There is a \n in the asctime already
}

void HomeEnvironment::loop()
{
	FEED_WDT();

	if (debugOutput && (millis() - statisticsInterval) > STATISTICS_INTERVAL)
	{
		statisticsInterval = millis();
		printStatistics();
	}

	// In each loop, make sure there is an Internet connection.
	if (WiFi.status() != WL_CONNECTED)
	{
		ESP_LOGI(LOG_TAG, "[%d]: WiFi connection lost, connecting", millis());
		// Continuing makes no sense since the code below requires WiFi/internet
		if (!connectToWifi())
			return;
	}

	if (!clock_set)
	{
		ESP_LOGV(LOG_TAG, "[%d]: Clock not set, setting", millis());
		clock_set = setClock();
	}

	if (!Config::host.isEmpty())
	{
		//ESP_LOGI(LOG_TAG, "Loop");

		// Disable pinging for now
		if ((millis() - pingInterval) > PING_INTERVAL_TIMING)
		{
			ESP_LOGV(LOG_TAG, "Pinging..");

			bool ret = Ping.ping(Config::host.c_str(), 2);
			avg_time_ms = Ping.averageTime();
			digitalWrite(Config::led, LOW);
			if (!ret)
			{
				pingFailureInterval++;
			}
			else
			{
				pingFailureInterval = 0;
				client->publish(Config::available_topic.c_str(), "1");
			}
			digitalWrite(Config::led, HIGH);
			pingInterval = millis();
		}

		if (pingFailureInterval > 5)
		{
			ESP_LOGV(LOG_TAG, "[%d]: Ping failed 5 times, reconnecting", millis());

			// Disconnect from everything and connect again
			client->disconnect();

			do
			{
				FEED_WDT();
				WiFi.reconnect();
			} while (WiFi.status() != WL_DISCONNECTED);

			if (!connectToWifi())
			{
				// Disconnect again
				WiFi.disconnect(1);
			}
			else
			{
				pingFailureInterval = 0;
			}
		}

		if (!client->connected() && ((millis() - connectionRetryInterval) > FAILED_RETRY_INTERVAL))
		{
			connectToMqtt();
		}

		// This may cause issues as the callbacks and subscribed topics are not properly restored.
		bool ok = lastLoop;
		if (((lastLoop = client->loop()), !lastLoop) && !ok && ((millis() - connectionRetryInterval) > FAILED_RETRY_INTERVAL))
		{
			connectionRetryInterval = millis();
			ESP_LOGE(LOG_TAG, "[%d]: MQTT Client loop failed, initializing new client", millis());

			// (hopefully) reset the client
			client->disconnect();

			//this->client = nullptr;
			//this->client = new PubSubClient(Config::host, this->port, this->secureClient);
		}
	}

	//ESP_LOGV(LOG_TAG, "Handling ArduinoOTA");
#ifndef DISABLE_OTA
	ArduinoOTA.handle();
#endif
}

uint8_t getClockFails = 0;

boolean HomeEnvironment::setClock()
{
	// Check if the time hasn't already been set
	if (time(nullptr) > 8 * 3600 * 2)
	{
		return true;
	}

	long clockSetTimeout = 0;

	// Delete the old one
	if (timeClient != nullptr)
		delete (timeClient);

	timeClient = new NTPClient(ntpUDP, Config::time_server.c_str(), 0, 60000);
	timeClient->begin();

	configTime(Config::time_gmt_offset, 0, Config::time_zone.c_str());

	setenv("TZ", Config::time_zone.c_str(), 1);

	ESP_LOGV(LOG_TAG, "Waiting for NTP time sync:");

	time_t now;

	if (getClockFails >= CLOCK_ACQ_FAIL_COUNT)
	{
		ESP_LOGE(LOG_TAG, "Failed to set clock");
		getClockFails = 0;
		return false;
	}

	do
	{
		FEED_WDT();

		// This is much faster
		if (timeClient->update())
		{
			clockSetTimeout = millis();

			struct timespec currentTime;
			currentTime.tv_sec = timeClient->getEpochTime();

			settimeofday((struct timeval *)&currentTime, NULL);

			now = time(nullptr);
		}

		// This needs to outside of the if statement above, since otherwise it will not get called
		// Causing a WDT reset
		if ((millis() - clockSetTimeout) > FAILED_RETRY_INTERVAL)
		{
			getClockFails++;
			clockSetTimeout = millis();
			return setClock();
		}

		delay(10);
	} while (now < 8 * 3600 * 2);

	struct tm timeinfo;
	gmtime_r(&now, &timeinfo);

	ESP_LOGV(LOG_TAG, "Current time: %s", asctime(&timeinfo));

	return true;
}

long wifiConnectTimeout;

boolean HomeEnvironment::connectToMqtt()
{
	if (client->connected())
		return true;

	if ((WiFi.status() != WL_CONNECTED && connectToWifi()) || WiFi.status() == WL_CONNECTED)
	{
		ESP_LOGW(LOG_TAG, "[%d]: MQTT Client not connected, connecting (rc=%d)", millis(), client->state());

		IPAddress resolvedIP;
		if (!WiFi.hostByName(Config::host.c_str(), resolvedIP))
		{
			ESP_LOGE(LOG_TAG, "[%d]: Could not lookup hostname!", millis());
		}
		else
		{
			ESP_LOGI(LOG_TAG, "[%d]: Connecting to: %s", millis(), resolvedIP.toString().c_str());

			secureClient.setTimeout(20000);
			client->setSocketTimeout(20000);

			if (client->connect(Config::client_id.c_str(), Config::available_topic.c_str(), 0, true, "0"))
			{
				ESP_LOGI(LOG_TAG, "[%d]: Connected to: %s (rc=%d)", millis(), resolvedIP.toString().c_str(), client->state());

				lastActivity = 0;
				// Connected
				client->publish(Config::available_topic.c_str(), "1"); // Availibility is always off when the device boots

				if (connection_callback != NULL)
					connection_callback(this);

				return true;
			}
			else if ((millis() - lastActivity) > reconnectTimeout)
			{
				ESP_LOGE(LOG_TAG, "[%d]: Failed to connect the MQTT Client, connecting (rc=%d)", millis(), client->state());
				// Keep trying
				// freezeWithOTA("Could not reconnect to the server");
			}
			else
			{
				if (lastActivity == 0)
				{
					lastActivity = millis();
				}
				ESP_LOGE(LOG_TAG, "[%d]: Could not (re-)connect to the MQTT server", millis());
			}
		}
	}
	return false;
}

boolean HomeEnvironment::connectToWifi()
{
	ESP_LOGV(LOG_TAG, "[%d]: Connecting to WiFi", millis());

	WiFi.mode(WIFI_STA);
	WiFi.begin(Config::ssid.c_str(), Config::pass.c_str());

	wifiConnectTimeout = millis();

	while (WiFi.status() != WL_CONNECTED)
	{
		FEED_WDT();

		if (!deepSleepEnabled)
		{
			digitalWrite(Config::led, !digitalRead(Config::led));
		}
		if ((millis() - wifiConnectTimeout) > 20000)
		{
			if (WiFi.status() == WL_CONNECT_FAILED)
			{
				ESP_LOGV(LOG_TAG, "[%d]: WiFi connection failed. Possibly wrong password", millis());
			}

			ESP_LOGV(LOG_TAG, "[%d]: Connecting took to long, disconnecting and trying again", millis());

			WiFi.reconnect();
			wifiConnectTimeout = millis();

			if (!connectToWifi())
			{
				// Disconnect again
				WiFi.disconnect(1);
			}
			else
			{
				pingFailureInterval = 0;
			}
		}
	}

#if defined(ESP8266) || defined(ESP8285)
	WiFi.hostname(Config::client_id.c_str());
	WiFi.setSleepMode(WIFI_NONE_SLEEP);
#endif
#ifdef ESP32
	if (!WiFi.setHostname(Config::client_id.c_str()))
		ESP_LOGE(LOG_TAG, "Failed to set hostname to %s", Config::client_id.c_str());
#endif

	digitalWrite(Config::led, WiFi.status() == WL_CONNECTED);
	return WiFi.status() == WL_CONNECTED;
}

void HomeEnvironment::freezeWithOTA(char *reason)
{
	long blinkDelay = millis();
	while (true)
	{
		if ((millis() - blinkDelay) > 100)
		{

			ESP_LOGV(LOG_TAG, "Freezing with OTA because of: %s", reason);
			FEED_WDT();
			digitalWrite(Config::led, !digitalRead(Config::led));
			blinkDelay = millis();
		}
		yield();
	}
}

// https://arduinojson.org/v6/how-to/merge-json-objects/
void merge(JsonVariant dst, JsonVariantConst src)
{
  if (src.is<JsonObjectConst>())
  {
    for (auto kvp : src.as<JsonObjectConst>())
    {
      merge(dst.getOrAddMember(kvp.key()), kvp.value());
    }
  }
  else
  {
    dst.set(src);
  }
}


bool HomeEnvironment::getConfig(DynamicJsonDocument &root)
{
	bool ok = false;

	if ((ok = FSPROVIDER.exists(CONFIG_FILE), !ok))
	{
		ESP_LOGW("", "Config file not existing");
		return ok;
	}

	File file = FSPROVIDER.open(CONFIG_FILE, "r+");

	size_t size = file.size();

	ESP_LOGD("", "Config file size %d", size);

	if ((ok = (size > 0), ok))
	{
		ReadBufferingStream bufferedFile(file, size);
		DeserializationError errorCode = deserializeJson(root, bufferedFile);
		ok = (errorCode == DeserializationError::Code::Ok);
		if (!ok)
			ESP_LOGE("", "Error deserializing json %s", errorCode.c_str());
	}
	else
	{
		ESP_LOGW("", "Config file empty");
	}

	file.close();

	return ok;
}

bool HomeEnvironment::updateConfig(char *buffer, size_t length, bool dryRun)
{
	bool ok = false;

	std::unique_ptr<char[]> newBuf(new char[length]);
	memset(newBuf.get(), '\0', length); // isEmpty the buffer

	DynamicJsonDocument root(4096);
	DeserializationError errorCode;
	ok = (errorCode = deserializeJson(root, newBuf.get()), errorCode) == DeserializationError::Code::Ok;

	if (ok)
	{
		return updateConfig((JsonObject &)root, dryRun);
	}
	else
	{
		publish(Config::otaConfigState.c_str(), String((dryRun ? String("Config update would fail: ") : String("Failed updating config: ")) + String(errorCode.c_str())).c_str());
		return ok;
	}
	return ok;
}

bool HomeEnvironment::updateConfig(JsonObject &root, bool dryRun)
{
	bool ok = false;

	DynamicJsonDocument originalConfig(4096);
	if ((ok = getConfig(originalConfig), ok))
	{
		merge((JsonVariant &)originalConfig, root);
		if (!dryRun)
			ok = writeConfig(originalConfig);

		if (ok)
		{
			publish(Config::otaConfigState.c_str(), (dryRun ? String("Would have updated the config") : String("Updated config")).c_str());
		}
	}

	return ok;
}

bool HomeEnvironment::writeConfig(DynamicJsonDocument &root)
{
	bool ok = false;

	size_t measuredJsonSize = measureJson(root) + 1;

	std::unique_ptr<char[]> newBuf(new char[measuredJsonSize]);
	memset(newBuf.get(), '\0', measuredJsonSize); // isEmpty the buffer

	serializeJson(root, newBuf.get(), measuredJsonSize);

	File file = FSPROVIDER.open(CONFIG_FILE, "w+");

	size_t writtenBytes = file.write((const uint8_t *)newBuf.get(), measuredJsonSize);

	ok = writtenBytes == measuredJsonSize;

	file.flush();
	file.close();

	// Reboot here?

	return ok;
}

bool HomeEnvironment::otaConfigCallback(uint8_t *load, unsigned int length)
{
	bool ok = false;

	// Update the config
	if (updateConfig((char *)load, (size_t)length))
	{
		loop();

		// Reboot
		ESP.restart();
	}

	return ok;
}

String HomeEnvironment::getOtaConfigTopic()
{
	return Config::otaConfigTopic;
}

bool HomeEnvironment::setCallback(MQTT_CALLBACK_SIGNATURE)
{
	if (client == nullptr)
		return false;

	this->callback = callback;
	return this->client == &client->setCallback(callback);
}

String HomeEnvironment::getClientId()
{
	return Config::client_id;
}

boolean HomeEnvironment::publish(const char *topic, const char *payload)
{
	if (client == nullptr)
		return false;
	return client->publish(topic, payload);
}

boolean HomeEnvironment::publish(const char *topic, const char *payload, boolean retained)
{
	if (client == nullptr)
		return false;
	return client->publish(topic, payload, retained);
}

boolean HomeEnvironment::publish(const char *topic, const uint8_t *payload, unsigned int plength)
{
	if (client == nullptr)
		return false;
	return client->publish(topic, payload, plength);
}

boolean HomeEnvironment::publish(const char *topic, const uint8_t *payload, unsigned int plength, boolean retained)
{
	if (client == nullptr)
		return false;
	return client->publish(topic, payload, plength, retained);
}

boolean HomeEnvironment::publish_P(const char *topic, const char *payload, boolean retained)
{
	if (client == nullptr)
		return false;
	return client->publish_P(topic, payload, retained);
}

boolean HomeEnvironment::publish_P(const char *topic, const uint8_t *payload, unsigned int plength, boolean retained)
{
	if (client == nullptr)
		return false;
	return client->publish_P(topic, payload, plength, retained);
}

boolean HomeEnvironment::beginPublish(const char *topic, unsigned int plength, boolean retained)
{
	if (client == nullptr)
		return false;
	return client->beginPublish(topic, plength, retained);
}

int HomeEnvironment::endPublish()
{
	if (client == nullptr)
		return 0;
	return client->endPublish();
}

size_t HomeEnvironment::write(uint8_t data)
{
	if (client == nullptr)
		return 0;
	return client->write(data);
}

size_t HomeEnvironment::write(const uint8_t *buffer, size_t size)
{
	if (client == nullptr)
		return 0;
	return client->write(buffer, size);
}

boolean HomeEnvironment::subscribe(const char *topic)
{
	if (client == nullptr)
		return false;
	return client->subscribe(topic);
}

boolean HomeEnvironment::subscribe(const char *topic, uint8_t qos)
{
	if (client == nullptr)
		return false;
	return client->subscribe(topic, qos);
}

boolean HomeEnvironment::unsubscribe(const char *topic)
{
	if (client == nullptr)
		return false;
	return client->unsubscribe(topic);
}